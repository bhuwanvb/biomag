﻿using UnityEngine;
using System.Collections;

public class CorrespondingPair 
{
	public string Code;
	public string Name;
	public string Description;
	public string GermUserFriendlyCode;
	public string Location;
	public string RespectiveScanPoint;
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CorrespondingPointGameObject : MonoBehaviour {

		public GameObject Pivot;
		public AccuCamera PointManager;
		public GameObject DropDown;

		public enum PivotDirection
		{
				front, back, left, right
		}
		public PivotDirection pd;

		// Use this for initialization
		void Start () 
		{
				PointManager = Camera.main.GetComponent<AccuCamera> ();
				CreatePivot ();
		}

		// Update is called once per frame
		void Update () 
		{

		}

		void OnDestroy()
		{
				
		}
				

		public void OnMouseDown()
		{
				Debug.Log ("Clicked the object : "+ gameObject.name );
				switch (pd)
				{
				case PivotDirection.front:
						Pivot.transform.rotation = Quaternion.identity;
						break;
				case PivotDirection.left:
						Pivot.transform.rotation = Quaternion.Euler(0,90,0);
						break;
				case PivotDirection.back:
						Pivot.transform.rotation = Quaternion.Euler(0,180,0);
						break;
				case PivotDirection.right: 
						Pivot.transform.rotation = Quaternion.Euler(0,270,0);
						break;
						//				case 'c':
						//					Pivot.transform.rotation = Quaternion.Euler(0,310,0);
						//					break;
				default:
						break;
				}
		}

		public void OnFocus(){

				switch (pd) {
				case PivotDirection.front:
						Pivot.transform.rotation = Quaternion.identity;
						break;
				case  PivotDirection.left:
						Pivot.transform.rotation = Quaternion.Euler(0,90,0);
						break;
				case  PivotDirection.back:
						Pivot.transform.rotation = Quaternion.Euler(0,180,0);
						break;
				case  PivotDirection.right:
						Pivot.transform.rotation = Quaternion.Euler(0,270,0);
						break;
						//				case 'c':
						//						Pivot.transform.rotation = Quaternion.Euler(0,310,0);
						//						break;
				default:
						break;
				}
				PointManager.GetFocusTargetPoint(Pivot.transform);
		}

		void CreatePivot()
		{
				Pivot = new GameObject ();
				Pivot.name = "pivObj_"+gameObject.name;
				Pivot.transform.parent = transform;
				Pivot.transform.rotation = Quaternion.identity;
				Pivot.transform.localPosition = Vector3.zero;
				Pivot.transform.localScale = Vector3.one * 0.0553507f;
		}

		void DeleteAllChildren(GameObject parentGO)
		{
				foreach (Transform child in parentGO.transform)
				{
						Destroy(child.gameObject);
				}
		}
}

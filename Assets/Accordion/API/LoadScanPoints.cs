﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
//using Newtonsoft.Json;
using LitJson;

public class LoadScanPoints : MonoBehaviour {
	
	public string TestData = "";
	public string data = "";
	//public string ScanPointUrl = "http://prithiviraj.vmokshagroup.com:8089/AnatomicalBiomagneticMatrix";
	public string ScanPointUrl = "http://prithiviraj.vmokshagroup.com:8089/Search/AnatomicalBiomagneticMatrix";
	//public string ScanPointUrl = "http://biomagnetictherapy.us-west-2.elasticbeanstalk.com/Search/AnatomicalBiomagneticMatrix";
	public string req; 
	
	public BioMagneticPoint bioMagneticPoint;
	public GameObject SectionUI;
	public GameObject SectionTitleUI;
	public GameObject ScanPointUI;
	public GameObject Accordion;
	public GameObject DescriptionPanel;
	public GameObject DescriptionPanelBtn;
	public GameObject ScanPointDesPref;
	public GameObject CorrespondingPairDesPref;
	
	
	System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding(); 
	
	
	private static LoadScanPoints Instance;
	
	public static LoadScanPoints instance
	{
		get
		{
			if(Instance == null)
				return GameObject.FindObjectOfType<LoadScanPoints>();
			
			return Instance;
		}
	}
	
	public delegate void LoadedData();
	public LoadedData IntimateScanPoints;
	public GameObject[] CorrespondingPoints;
	
	public Dictionary<string, List<ScanPoint>> Section_Scanpoint_Dict = new Dictionary<string, List<ScanPoint>>();
	public Dictionary<string, List<CorrespondingPair>> Scanpoint_cPair_Dict = new Dictionary<string, List<CorrespondingPair>>();
	//public Dictionary<string, List<CorrespondingPair>> Scanpoint_cPair_Dict = new Dictionary<string, List<CorrespondingPair>>();
	// Use this for initialization
	void Start () 
	{
		bioMagneticPoint = new BioMagneticPoint ();
		StartCoroutine ( GetScanPoints());
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void Awake(){
		//to save data
		PlayerPrefs.SetString("TestData",TestData);
		PlayerPrefs.Save ();
		
		//to load data
		
		if (PlayerPrefs.HasKey ("TestData")) {
			data = PlayerPrefs.GetString ("TestData");
		}
	}
	
	/// <summary>
	/// Getting json response
	/// </summary>
	/// <returns>The scan points.</returns>
	IEnumerator GetScanPoints()
	{
		//Adding headers
		Dictionary<string, string> headers = new Dictionary<string, string>();
		headers.Add("Content-Type", "application/json");
		headers.Add("Accept", "application/json");
		
		
		
		//GET Json 
		WWW www = new WWW(ScanPointUrl, encoding.GetBytes(req), headers);
		yield return www;
		
		if (www.error != null)
		{
			Debug.Log("Error in downloading data !");
		}
		else
		{
	    	Debug.Log(www.text);
			Deserialize (www.text);
			
		}
	}
	
	public void Deserialize(string json)
	{
		//Deserialization of json response
		Wrapper wrapper = new Wrapper();
		wrapper.md =  JsonMapper.ToObject<MasterData>(json);
		Debug.Log (wrapper.md.aaData.AnatomicalBiomagneticMatrix.Count);
		
		foreach (BioMagneticPoint bioMagneticPoint  in wrapper.md.aaData.AnatomicalBiomagneticMatrix)
		{
			//Creating new corresponting pair object 
			CorrespondingPair correspondingPair = new CorrespondingPair();
			correspondingPair.Code = bioMagneticPoint.CorrespondingPairCode;
			correspondingPair.Name = bioMagneticPoint.CorrespondingPair;
			correspondingPair.RespectiveScanPoint = bioMagneticPoint.ScanPointCode;
			correspondingPair.Description = bioMagneticPoint.Description;
			correspondingPair.Location = bioMagneticPoint.LocationCorrespondingPair;
			correspondingPair.GermUserFriendlyCode = bioMagneticPoint.GermsUserFriendlyCode;
			
			//Adding ScanPointCode as key and correspondingPair as value to dictionary (Scanpoint_cPair_Dict), where ScanPointCode != null
			if(bioMagneticPoint.ScanPointCode != null)
			{
				if (Scanpoint_cPair_Dict.ContainsKey (bioMagneticPoint.ScanPointCode)) 
				{
					//if scanpoint already exists, add the corresponding pair to the value, which is a list of <CorrespondingPair>					
					Scanpoint_cPair_Dict [bioMagneticPoint.ScanPointCode].Add (correspondingPair);
				} 
				else 
				{
					//if scanpoint does not exists in the dictionary, add (ScanPointCode, List<correspondingPair>)
					List<CorrespondingPair> tempCPList = new List<CorrespondingPair> ();
					tempCPList.Add (correspondingPair);
					Scanpoint_cPair_Dict.Add (bioMagneticPoint.ScanPointCode, tempCPList);
				}
			}
			
			
			//Creating new ScanPoint Object
			ScanPoint sPoint = new ScanPoint ();
			sPoint.Code = bioMagneticPoint.ScanPointCode;
			sPoint.Name = bioMagneticPoint.ScanPoint;
			sPoint.SectionCodeBelonging = bioMagneticPoint.SectionCode;
			sPoint.Location = bioMagneticPoint.LocationScanPoint;
			sPoint.CPairs.Add (correspondingPair);
			
			//adding SectionCode<string> as key and List of ScanPoint Objects as value to dictionary named Section_Scanpoint_Dict
			if (bioMagneticPoint.SectionCode != null) 
			{
				//if SectionCode already exists, add the ScanPoint object to the value, which is a list of <ScanPoint>	
				if (Section_Scanpoint_Dict.ContainsKey (bioMagneticPoint.SectionCode))
				{
					//Check if the list is having any scanpoint object with the same name as the current scanpoint object. If not, add the scanpoint object to the list.
					//This prevents redundant scanpoint in the list
					if(Section_Scanpoint_Dict [bioMagneticPoint.SectionCode].Any(record => record.Name == sPoint.Name) == false)
						Section_Scanpoint_Dict [bioMagneticPoint.SectionCode].Add (sPoint);
				} 
				else
				{
					//if SectionCode does not exist in the dictionary, add (SectionCode, scanpoint<list>) to it
					List<ScanPoint> tempSPList = new List<ScanPoint> ();
					tempSPList.Add (sPoint);
					Section_Scanpoint_Dict.Add (bioMagneticPoint.SectionCode, tempSPList);
				} 
			}
			
			IntimateScanPoints ();
			
		}
		
		//Debug.Log (Scanpoint_cPair_Dict.Count);
		
		List<string> sections = GetSectionsName ();
		
		//To create Dictionary UI in Runtime
		foreach (string str in sections) 
		{
			//Debug.Log (str);
			
			//Instantiating required prefabs
			GameObject sectionUI = Instantiate (SectionUI);
			GameObject sectionTitleUI = Instantiate (SectionTitleUI);
			
			
			//Assigning SectionName
			sectionTitleUI.GetComponent<Text> ().text = str;
			
			//Forming the regired hierarchy
			sectionTitleUI.transform.SetParent(sectionUI.transform,false);
			sectionUI.transform.SetParent(Accordion.transform,false);
			
			//Populating scanpoint data under each sections
			CreateScanPointUI (str, sectionUI);
		}
		
		
		
		//		List<ScanPoint> sp = GetScanPointsForSection ("OTHER");
		//		foreach (ScanPoint p in sp) 
		//		{
		//			Debug.Log (p.Name);
		//		}
		
		//		List<CorrespondingPair> sp = GetCorrespondingPairs ("");
		//		foreach (CorrespondingPair p in sp) 
		//		{
		//			Debug.Log (p.Name);
		//		}
		
		//		foreach(var item in Scanpoint_cPair_Dict)
		//		{
		//			string cp = "";
		//			foreach (CorrespondingPair cPoint in item.Value)
		//			{
		//				cp += cPoint.Name + ", ";
		//			}
		//
		//			Debug.Log ("Scan point name : "+ item.Key +" corresponding pair : "+ cp);
		//
		//		}
	}
	
	/// <summary>
	/// returns the name of the sections as list.
	/// </summary>
	/// <returns>The sections name.</returns>
	public List<string> GetSectionsName()
	{
		return new List<string> (Section_Scanpoint_Dict.Keys);
	}
	
	/// <summary>
	/// returns all scan points names as list.
	/// </summary>
	/// <returns>The all scan points names.</returns>
	public List<string> GetAllScanPointsNames()
	{
		return new List<string> (Scanpoint_cPair_Dict.Keys);
	}
	
	/// <summary>
	/// returns the scan points for given section.
	/// </summary>
	/// <returns>The scan points for section.</returns>
	/// <param name="SectionName">Section name.</param>
	public List<ScanPoint> GetScanPointsForSection(string SectionName)
	{
		return Section_Scanpoint_Dict [SectionName];
	}
	
	/// <summary>
	/// Returns the corresponding pairs list for given scanpoint.
	/// </summary>
	/// <returns>The corresponding pairs.</returns>
	/// <param name="ScanpointName">Scanpoint name.</param>
	public List<CorrespondingPair> GetCorrespondingPairs(string ScanpointName)
	{
		if (ScanpointName != null)
			return Scanpoint_cPair_Dict [ScanpointName];
		else
			return	new  List<CorrespondingPair> ();
	}
	
	public void CreateScanPointUI(string sectionName, GameObject parentUI)
	{
		//Get list of scanPoints under the given section
		List <ScanPoint> scanPoints = GetScanPointsForSection (sectionName);
		foreach(ScanPoint sp in scanPoints)
		{
			//Instantiating scanpoint UI for each scanpoint
			GameObject sPointUI = Instantiate (ScanPointUI);
			
			//Assinging the name 
			sPointUI.GetComponent<Text> ().text = sp.Name;
			
			//Add a instance of DictionaryScanPoints script to each sPointUI gameobject
			//This script handles the onclick event listener for each scanpoint
			if (!sPointUI.GetComponent<ScanPointsEventListener> ()) 
			{
				sPointUI.AddComponent<ScanPointsEventListener> ();
				ScanPointsEventListener DictionaryScanPointsInstance = sPointUI.GetComponent<ScanPointsEventListener> ();
				DictionaryScanPointsInstance.SPoint = sp;
				DictionaryScanPointsInstance.CPoints =  GetCorrespondingPairs (sp.Code);
				//sPointUI.GetComponent<DictionaryScanPoints> ().SPoint = sp;
				//sPointUI.GetComponent<DictionaryScanPoints> ().CPoints = GetCorrespondingPairs (sp.Code);
			}
			
			//Forming the required heirarchy
			sPointUI.transform.SetParent(parentUI.transform,false);
		}
	}
	
	
}

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class ScanPointsEventListener : MonoBehaviour 
{
	public ScanPoint SPoint;
	public List<CorrespondingPair> CPoints;
	public GameObject DescriptionPanel;
	public GameObject DescriptionPanelBtn;
	public GameObject ScanPointDesPref;
	public GameObject CorrespondingPairDesPref;
	public Transform DescriptionPanelChild;
	// Use this for initialization
	void Start () 
	{
		DescriptionPanel = LoadScanPoints.instance.DescriptionPanel;
		DescriptionPanelBtn = LoadScanPoints.instance.DescriptionPanelBtn;
		ScanPointDesPref = LoadScanPoints.instance.ScanPointDesPref;
		CorrespondingPairDesPref = LoadScanPoints.instance.CorrespondingPairDesPref;
		DescriptionPanelChild = DescriptionPanel.transform.GetChild (0);
		gameObject.GetComponent<Button> ().onClick.AddListener (() => { ScanPointTouchEvent(); });
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void ScanPointTouchEvent()
	{
		string cp = "";
		DescriptionPanel.SetActive (true);
		DescriptionPanelBtn.SetActive (true);
		//instantiate scanpoint description
		GameObject scanPointDesPref = Instantiate(ScanPointDesPref);
		GameObject Scanpointdes =  scanPointDesPref.transform.GetChild(1).gameObject;
		Scanpointdes.GetComponentInChildren<Text> ().text = SPoint.Name;
		string loc = "Location Undefined";
		if (SPoint.Location != null)
						loc = SPoint.Location;

		GameObject Scanpointloc =  scanPointDesPref.transform.GetChild(3).gameObject;
		Scanpointloc.GetComponentInChildren<Text> ().text = loc;
		//assign instance of sp .getchild(0,1).text = spoint.name
		//assign instance of sp .getchild(3,4).text = spoint.location
		scanPointDesPref.transform.SetParent (DescriptionPanelChild, false);
		foreach (CorrespondingPair cPoint in CPoints)
		{
				cp += cPoint.Name + ", ";
		//	Instantiate cp ui 
			GameObject correspondingPairDesPref = Instantiate(CorrespondingPairDesPref);

			GameObject CorrespName =  correspondingPairDesPref.transform.GetChild(0).gameObject;
			CorrespName.GetComponent<Text> ().text = cPoint.Name;

			GameObject CorrespLocation =  correspondingPairDesPref.transform.GetChild(2).gameObject;
			CorrespLocation.GetComponent<Text> ().text = cPoint.Location;

			GameObject CorrespDescription =  correspondingPairDesPref.transform.GetChild(4).gameObject;
			CorrespDescription.GetComponent<Text> ().text = cPoint.Description;

			GameObject CorrespGerms =  correspondingPairDesPref.transform.GetChild(6).gameObject;
			CorrespGerms.GetComponent<Text> ().text = cPoint.GermUserFriendlyCode;
		

			correspondingPairDesPref.transform.SetParent (DescriptionPanelChild, false);
		}

		Debug.Log ("Scan point name : "+ SPoint.Name +" corresponding pair : "+ cp);
		//
	}
}

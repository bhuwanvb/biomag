﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraLookAt : MonoBehaviour {
	public Transform TargetLookAt,primary;    //target to which camera will look always
	public float Distance=20f;         //distance of camera from target
	
	public float DistanceSmooth=0.05f;    //smoothing factor for camera movement
	public float X_Smooth=0.05f;
	public float Y_Smooth=0.1f;
	public float height=35f;

	public float rotX=0f;        
	
	private float speed=80f;
	private float desiredDistance=0f; //desired distance of camera from target
	private float velDistance=0f;    
	private Vector3 desiredPosition=Vector3.zero; 
	private float velX=0f;
	private float velY=0f;
	private float velZ=0f;
	private Vector3 position=Vector3.zero;
	public float rotateDelta;
	public bool isMouse;

    public List<Transform> Points;

    public Transform mid, head, lh, rh, ll, rl;
	public void OnEnable(){
		rotX = 180;
		height = 6;
        position = transform.position;
        Distance = 2000;
        HidePoints();
        TargetLookAt = primary;
	}

	void LateUpdate () 
	{
		if(TargetLookAt==null)
			return;
		//AngleCalc();
		rotateRight (rotateDelta);

		if (!isMouse) {
			
			if (Input.touchCount == 1 && Input.GetTouch (0).phase == TouchPhase.Moved) {
				if (Mathf.Abs (Input.GetTouch (0).deltaPosition.y) > Mathf.Abs (Input.GetTouch (0).deltaPosition.x)) {
					height += Input.GetTouch (0).deltaPosition.y * Time.deltaTime * 5;
					//height=Mathf.Clamp(height,0,15);
				} else
					rotateRight (Input.GetTouch (0).deltaPosition.x * 25);
			}

		} else {
			if (Input.GetKey (KeyCode.Mouse0)) {
				if (Mathf.Abs (MouseHelper.mouseDelta.y) > Mathf.Abs (MouseHelper.mouseDelta.x)) {
					height += MouseHelper.mouseDelta.y * Time.deltaTime * 5;
					//height=Mathf.Clamp(height,0,15);
				} else
					rotateRight (MouseHelper.mouseDelta.x * 25);

			}

		}
		CalculateDesiredPosition();
		UpdatePosition();
		//height=Mathf.Clamp(height,5,35);
	}
	
	
	void AngleCalc()
	{
		
		rotX+=Time.deltaTime*speed;
		
	}

	void rotateRight(float magnitude){
		rotX+=Time.deltaTime*magnitude;
	}
	
	void CalculateDesiredPosition()
	{
        desiredPosition = CalculatePosition(height, rotX, Distance);

        //desiredPosition = Vector3.MoveTowards(desiredPosition, temp, Time.deltaTime * 0.0005f);
	}
	
	Vector3 CalculatePosition(float rotationX, float rotationY, float distance)
	{
		Vector3 direction=new Vector3(0,0,-distance);
		Quaternion rotation=Quaternion.Euler(rotationX,rotationY,0);
		return TargetLookAt.position+rotation*direction;
	}
	
	void UpdatePosition()
	{
        var posX = Mathf.SmoothDamp(position.x, desiredPosition.x, ref velX, X_Smooth);
		var posY=Mathf.SmoothDamp(position.y,desiredPosition.y,ref velY,Y_Smooth);
		var posZ=Mathf.SmoothDamp(position.z,desiredPosition.z,ref velZ,X_Smooth);
		
		position=new Vector3(posX,posY,posZ);

		transform.position=position;
		transform.LookAt(TargetLookAt);
	}

    void OnHead()
    {
        Distance = 1000;
        TargetLookAt = head;
    }

    void OnLeftHand()
    {
        Distance = 1000;
        TargetLookAt = lh;
    }

    void OnRightHand()
    {
        Distance = 1000;
        TargetLookAt = rh;
    }
    void OnLeftLeg()
    {
        Distance = 1000;
        TargetLookAt = ll;
    }
    void OnRightLeg()
    {
        Distance = 1000;
        TargetLookAt = rl;
    }

    void OnMid()
    {
        Distance = 1000;
        TargetLookAt = mid;
    }

    public void GetFocusTargetPoint(Transform target)
    {

        Distance = 1000;
        TargetLookAt = target;

        //if (target.rotation.eulerAngles.x > 260 && target.rotation.eulerAngles.x < 280)
        //    rotX = 180;
        //else
        //    rotX = 0;
    }

    public void OnShowPoints()
    {
        foreach (Transform pt in Points)
            pt.gameObject.SetActive(true);

    }

    public void HidePoints()
    {

        foreach (Transform pt in Points)
            pt.gameObject.SetActive(false);

    }
}

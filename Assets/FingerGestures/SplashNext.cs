﻿using UnityEngine;
using System.Collections;

public class SplashNext : MonoBehaviour {
	public int delayTime=0;
	// Use this for initialization
	void Start () {
		Invoke ("LoadSplash",delayTime);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void LoadSplash(){
		Application.LoadLevel (Application.loadedLevel + 1);
	}
}

﻿using UnityEngine;
using System.Collections;

public class AnimatorAssembly : MonoBehaviour {
		 
		public Animator LoginAnimator,NavigationAnimator,BottomMenuAnimator,SlideMenuAnimator;


	
		public static AnimatorAssembly AnimatorController; 
	// Use this for initialization
	void Start () {
				AnimatorController = this;	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	// PLay Login Animation
		public void PlayLoginAnimation(){
				LoginAnimator.SetTrigger ("OnLoignClick");
		}

		// On Back on Logout Click
		public void PlayLogOutClickAnimation(){
				LoginAnimator.SetTrigger ("ReverseLogin");
		}

   // Navigation Click
		public void NavigationOnAnimation(){
				NavigationAnimator.SetTrigger ("NaviagtionON");
		}

	// Navigation OFF
		public void NavigationOFFAnimation(){
				NavigationAnimator.SetTrigger ("NavigationOFF");
		}

    // BottomMenuAnimator ON
		public void BottomMenuON(){
				BottomMenuAnimator.SetTrigger ("BottomMenuON");
		}

		// BottomMenuAnimator OFF
		public void BottomMenuOFF(){
				BottomMenuAnimator.SetTrigger ("BottomMenuOFF");
		}

		// SldeMenu ON
		public void SlideMenuMenuON(){
				SlideMenuAnimator.SetTrigger ("SlideMenuON");
		}

		//SldeMenu OFF
		public void SlideMenuOFF(){
				SlideMenuAnimator.SetTrigger ("SlideMenuOFF");
		}
}

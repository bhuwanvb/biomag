﻿using UnityEngine;
using System.Collections;

public class OnMouseClickClass : MonoBehaviour {
		public GameObject Pivot;
		public ScanPointPlayer ScanPlayer;
		public PivotDirection pd;
		public enum PivotDirection
		{
				front, back, left, right
		}
	// Use this for initialization
	void Start () {
				ScanPlayer = GameObject.Find ("ScanPlayer").GetComponent<ScanPointPlayer> ();
//				Debug.Log ("Call");
				CreatePivot ();
	}
	
	
		public void OnMouseDown(){
				CallForFocus ();
		}

		public void CallForFocus(){
				switch (pd)
				{
				case PivotDirection.front:
						Pivot.transform.rotation =  Quaternion.identity;
						break;
				case PivotDirection.left:
						Pivot.transform.rotation = Quaternion.Euler(0,90,0);
						break;
				case PivotDirection.back:
						Pivot.transform.rotation = Quaternion.Euler(0,180,0);
						break;
				case PivotDirection.right: 
						Pivot.transform.rotation = Quaternion.Euler(0,270,0);
						break;
				default:
						break;
				}
				ScanPlayer.FocusScanPoints (Pivot);
		}


		void CreatePivot()
		{
				Pivot = new GameObject ();
				Pivot.name = "pivObj_"+gameObject.name;
				Pivot.tag = "ScamPointPivot";
				Pivot.transform.parent = transform;
				Pivot.transform.rotation = Quaternion.identity;
				Pivot.transform.localPosition = Vector3.zero;
				Pivot.transform.localScale = Vector3.one * 0.0553507f;

				ScanPlayer.FindScanPointPivot (gameObject);
		}
}

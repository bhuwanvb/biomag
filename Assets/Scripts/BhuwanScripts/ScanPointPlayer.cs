﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ScanPointPlayer : MonoBehaviour {

		public AccuCamera PointManager;
		public GameObject ScanpointDropdown,ScanButtonPrefab,Content,rollingMenu;
		public List<GameObject> ScanPointsObjects,ScanPointsObjectsUnSort,ScanButtons;
		public int ButtonIndex,j,i;
		public bool PlayScan;
		public FocusBodyParts focusbodyparts;
	// Use this for initialization


	void Start () {
				PlayScan = false;
				ScanButtonPrefab = Resources.Load ("Prefabs/ScanButton") as GameObject;
				PointManager = GameObject.Find ("Main Camera").GetComponent<AccuCamera> ();
				ScanpointDropdown = EventManager.instance.ScanpointDropdown;
	}

		// Making the list of the of Pivot
		public void FindScanPointPivot(GameObject ScanPoint){
				ScanPointsObjects.Add (ScanPoint);
				ScanButtons.Add((GameObject)Instantiate (ScanButtonPrefab));
				ScanButtons[ButtonIndex].transform.SetParent(Content.transform,false);
			//	Content.GetComponent<RectTransform> ().sizeDelta = new Vector2 (Content.GetComponent<RectTransform> ().sizeDelta.x, Content.GetComponent<RectTransform> ().sizeDelta.y + ScanButtons [ButtonIndex].GetComponent<RectTransform> ().sizeDelta.y);
				ScanButtons [ButtonIndex].GetComponentInChildren<Text>().text = ScanPoint.name;
				ButtonIndex++;
		}



		// Will return select ScanPointIndex
		public void FindIndexOfScanPoint(GameObject currentObject){
				for (int k = 0; k < ScanPointsObjects.Count; k++) {
						if (currentObject.name == ScanPointsObjects[k].name) {
								//return i;
								//break;
						}
				}
		}

		// To make J=0;
		public void ResetThePlayPosition(){
				PlayScan = !PlayScan;
				j = 0;

		}
				
		// Called on the selection of ScanPoints
		public void FocusPointplayer(){

				if (PlayScan == true) {
						if (rollingMenu.activeSelf == false) {
								ScanpointDropdown.SetActive (true);
								rollingMenu.SetActive (true);
						}

						for (i = 0; i < ScanPointsObjects.Count; i++) {
								if (ScanPointsObjects [i].GetComponent<ScanPointID> ().ScnPointID == j) {
										ScanPointsObjects [i].GetComponent<MeshRenderer> ().enabled = true;
										ScanPointsObjects [i].GetComponent<OnMouseClickClass> ().CallForFocus ();
										break;
								}
						}
						StartCoroutine (PlayScanPlayer (4));
				}
				else if (PlayScan == false) {
						StopAllCoroutines ();
						ScanpointDropdown.SetActive (false);
						rollingMenu.SetActive (false);
						focusbodyparts.ResetButtonPressed (focusbodyparts.Body);
				}

			
		}

		// To be called after given seconds
		IEnumerator PlayScanPlayer(float t){
				if (j < ButtonIndex) {
						j++;
				} else {
						j = 0;
						StopAllCoroutines ();
				}
				yield return new WaitForSeconds (t);
				ScanPointsObjects [i].GetComponent<MeshRenderer> ().enabled = false;
				FocusPointplayer ();
	
		}

		// Called once need to be focus on ScanPoints
		public void FocusScanPoints(GameObject Pivot){
				{
                    	PointManager.limit = 120;
						PointManager.distance = 90;

						PointManager.GetFocusTargetPoint(Pivot.transform);
						ScanpointDropdown.SetActive (true);

						GameObject dropdown = ScanpointDropdown.transform.GetChild (0).gameObject;
						dropdown.transform.GetChild (0).gameObject.GetComponent<Text> ().text = Pivot.name;

				}

		}

}

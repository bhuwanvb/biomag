﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_Manager : MonoBehaviour {
	
	public CanvasGroup DictionaryCanvas;
	public AudioClip ClickSound;
		public AudioSource AudiSouce;
	// Use this for initialization
	void Start () {
			//	DictionaryCanvas = GameObject.Find ("DictionarCanvas").GetComponent<CanvasGroup> ();
				DisableDictionary ();
	}

    // To Diable Dictionary
		public void EnableDictionary(){
				DictionaryCanvas.alpha = 1f;
				DictionaryCanvas.interactable = true;
		}

	// To Enable Dictionary
		public void DisableDictionary(){
				DictionaryCanvas.alpha = 0f;
				DictionaryCanvas.interactable = false;
		}

     // To Play Click Sound
		public void PlayClickSound(){
				AudiSouce.PlayOneShot (ClickSound);	
		}
	// Update is called once per frame
	
}

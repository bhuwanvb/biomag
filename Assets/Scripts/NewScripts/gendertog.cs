﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;




public class gendertog : MonoBehaviour {
	public bool tog,PointTog;
	public GameObject female,FemaleScanPointGroup,FemaleCorrespondingPairs;
	public GameObject male,MaleScanPointGroup,MaleCorrespondingPairs;
	public GameObject CurrentScanPoint, CurrentCorrespondingPoints;
	public Button GenderToggle;
	public GameObject maleSidepannel;
	public GameObject femaleSidepannel;
	public FocusBodyParts focusbodyparts;
    public Sprite MaleIcon, FemaleIcon;
	//	MeshRenderer[] ScanPointsGroupChildren;
//	GameObject[] sp;
//	GameObject[] cp;

	//public GameObject femaleimage;


	// Use this for initialization
	void Start () {
		tog = true;
				PointTog = false;
				CurrentScanPoint = FemaleScanPointGroup;
				CurrentCorrespondingPoints = FemaleCorrespondingPairs;
				SwitchGenderPoints (PointTog);
				CurrentScanPoint = MaleScanPointGroup;
				CurrentCorrespondingPoints = MaleCorrespondingPairs;
				SwitchGenderPoints (PointTog);
		
	}


	// To manage all the corresponding Points
	
		public void SwitchGenderPoints(bool CurrentState){
				foreach (MeshRenderer Scan in CurrentScanPoint.GetComponentsInChildren<MeshRenderer> ()) {
						Scan.enabled = CurrentState;
				}
				foreach (MeshRenderer Correspond in CurrentCorrespondingPoints.GetComponentsInChildren<MeshRenderer> ()) {
						Correspond.enabled = CurrentState;
				}
		}

	//To Toggel Model and their respective Points;
		public void TogglePoints(){
				PointTog = !PointTog;
				SwitchGenderPoints (PointTog);
		}

	public void  modeltoggle(){
				// TO disable on Toggle Button
				if (CurrentScanPoint.GetComponentInChildren<MeshRenderer> ().enabled = true) {
						PointTog = false;
						SwitchGenderPoints (false);
				}

//		foreach (GameObject go in sp)
//		{
//			go.GetComponent<MeshRenderer> ().enabled = false;
//			go.GetComponent<BoxCollider> ().enabled = false;
//		}
//		
//		foreach (GameObject go in cp)
//		{
//			go.GetComponent<MeshRenderer> ().enabled = false;
//		}
		//ResetButton.onClick.AddListener (()=> { ResetButtonPressed(Body); });
		    focusbodyparts.ResetButtonPressed (focusbodyparts.Body);
				tog = !tog;
				if (tog) {
			            femaleSidepannel.SetActive (false);
						female.SetActive (false);
					    male.SetActive (true);
						GenderToggle.image.sprite = MaleIcon;
						// fixed by Bhuwan
						CurrentScanPoint = MaleScanPointGroup;
						CurrentCorrespondingPoints = MaleCorrespondingPairs;
			            maleSidepannel.SetActive(true);
				}
		if (tog == false) {
			femaleSidepannel.SetActive (true);
		    female.SetActive (true);
			male.SetActive (false);
			CurrentScanPoint = FemaleScanPointGroup;
			CurrentCorrespondingPoints = FemaleCorrespondingPairs;
			GenderToggle.image.sprite = FemaleIcon;  // fixed by Bhuwan
			maleSidepannel.SetActive(false);

		}
		}

		 


}

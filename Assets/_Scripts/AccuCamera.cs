using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AccuCamera : MonoBehaviour {
	public Transform TargetLookAt;
	public Transform primaryTarget;
	public float distance,rotX,rotY,smooth;
	public bool testbool;

	public Vector3 TargetRot;

	public List<Transform> Points,RedPoints;


	public GameObject ActivePart,ActivePoints;

	//public DetailsTab Details;
	public check scriptD,scriptN;

	public float limit = 2;

	GameObject tempPivot; 
	// Use this for initialization
	public void Start () {


	
		//script = GameObject.Find("tempdicM").GetComponent<check>();

		//distance = 1.1f;
		testbool = false;
		transform.parent.parent = null;
		//TargetLookAt = primaryTarget;
		TargetRot = Vector3.zero;
		if (ActivePart) {
			ActivePart.GetComponent<Collider> ().enabled = true;
			//ActivePart.GetComponent<UISprite>().color = Color.white;
		}

		PanPosition = Vector3.zero;

		ActivePoints = null;
		if(isPointsEnabled)
			OnShowPoints ();
		
		foreach (Transform pt in RedPoints)
			pt.gameObject.SetActive(false);

//		Details.gameObject.SetActive (false);

		tempPivot = new GameObject ();
	}
	
	// Update is called once per frame
	Vector3 PanPosition;
	Vector2 tempPosition;


	void Update () {

	    if (scriptD.che2 || scriptN.che2) {
						testbool = true;
				} else
						testbool = false;
		///to check bool of another script


		if (transform.parent.parent != TargetLookAt)
			transform.parent.parent = TargetLookAt;

		//distance = Mathf.Clamp (distance, 0.2f, 2);
		//print (MouseHelper.mousePosition.y);

		transform.parent.localPosition = Vector3.Lerp (transform.parent.localPosition, Vector3.zero, smooth * Time.deltaTime);

		transform.parent.localRotation = Quaternion.Lerp (transform.parent.localRotation, Quaternion.identity, smooth * Time.deltaTime);

		if(Mathf.Abs(transform.localPosition.z-distance)>limit)
			transform.localPosition = Vector3.Lerp (transform.localPosition, new Vector3(0,0 ,distance), smooth  * Time.deltaTime);

		if(PanPosition.x!=0||PanPosition.y!=0)
			transform.localPosition = Vector3.MoveTowards (transform.localPosition, new Vector3(PanPosition.x,PanPosition.y ,transform.localPosition.z), smooth * Time.deltaTime);

		if (Input.touchCount > 1) {
			if (Input.GetKeyDown (KeyCode.Mouse0)) {
				TargetRot =TargetLookAt.rotation.eulerAngles;
				tempPosition = MouseHelper.mousePosition;
			}


			if (Mathf.Abs (tempPosition.x - MouseHelper.mousePosition.x) > 5 || Mathf.Abs (tempPosition.y - MouseHelper.mousePosition.y) > 5) {

				PanPosition.x += MouseHelper.mouseDelta.x * Time.deltaTime * 0.007f;
				PanPosition.y -= MouseHelper.mouseDelta.y * Time.deltaTime * 0.007f;
			}

			return;
		}

		if (Input.GetKeyDown (KeyCode.Mouse0)) {
			TargetRot =TargetLookAt.rotation.eulerAngles;

		}

		//print (MouseHelper.mouseDelta.y);
		if (Input.GetKey (KeyCode.Mouse0)&&(Mathf.Abs(MouseHelper.mouseDelta.y)>5||Mathf.Abs(MouseHelper.mouseDelta.x)>5)) {
//			if (MouseHelper.mousePosition.y < 400)
//				return;

			if (Mathf.Abs (MouseHelper.mouseDelta.y) > Mathf.Abs (MouseHelper.mouseDelta.x) && testbool == false) {
				TargetRot.x += MouseHelper.mouseDelta.y * Time.deltaTime * -3; //3
				//height=Mathf.Clamp(height,0,15);
			} else
				if(testbool == false)
				TargetRot.y+= Time.deltaTime* MouseHelper.mouseDelta.x * 8; //8
			
		}

		//print (TargetRot.x);


		TargetRot.x = ClampAngle(TargetRot.x, -60, 60);


		TargetLookAt.rotation = Quaternion.Lerp(TargetLookAt.rotation, Quaternion.Euler(TargetRot.x, TargetRot.y, TargetRot.z), Time.deltaTime*5);
		//TargetLookAt.rotation = Quaternion.Euler (ClampAngle (TargetLookAt.rotation.eulerAngles.x, -90, 90), TargetLookAt.rotation.y, TargetLookAt.rotation.z);

		//if(Input.to

	}


	float ClampAngle (float angle, float min, float max) {
		if (angle > 180)
			angle -= 360; 


//		if (angle < 360)
//			angle += 360;
//		if (angle > 360)
//			angle -= 360;
		return Mathf.Clamp (angle, min, max);
	}



	float ClampAngle2 (float angle, float min, float max) {



				if (angle < 360)
					angle += 360;
				if (angle > 360)
					angle -= 360;
		return Mathf.Clamp (angle, min, max);
	} 



	public void GetFocusTargetPoint(Transform target)
	{	
		PanPosition = Vector3.zero;
		distance = -2f;
		TargetLookAt = target;
		TargetRot = TargetLookAt.rotation.eulerAngles;
	}

	public bool isPointsEnabled;
	public void OnShowPoints()
	{
		if (ActivePoints) {
			foreach (Transform pt in ActivePoints.GetComponent<PointsGroup>().SubPressurePoints)
				pt.gameObject.SetActive (true);
		} 
		else {
			foreach (Transform pt in Points)
				pt.gameObject.SetActive (true);
		}

		isPointsEnabled = true;
		
	}
	
//	public void HidePoints()
//	{
//		GetCameraOut ();
//		Details.gameObject.SetActive (false);
//		foreach (Transform pt in Points)
//			pt.gameObject.SetActive(false);
//
//		foreach (Transform pt in RedPoints)
//			pt.gameObject.SetActive(false);
//
//		isPointsEnabled = false;
//		
//	}

	public void GetCameraOut(){

		tempPivot.transform.position = transform.parent.position;
		tempPivot.transform.rotation = transform.parent.rotation;

		TargetLookAt = tempPivot.transform;
		transform.parent.parent = TargetLookAt;
	}



//	public void SelectBodyPart(GameObject fromObject, GameObject pivotObject, GameObject pointsObject){
//		GetCameraOut ();
//		Details.gameObject.SetActive (false);
//
//		PanPosition = Vector3.zero;
//		distance = 0.5f;
//		TargetLookAt = pivotObject.transform;
//		TargetRot = Vector3.zero;
//
//		if (ActivePart) {
//			ActivePart.GetComponent<UIButton> ().enabled = true;
//			ActivePart.GetComponent<UISprite>().color = Color.white;
//		}
//
//		ActivePart = fromObject;
//		ActivePart.GetComponent<UIButton> ().enabled = false;
//	//	ActivePart.GetComponent<Collider> ().enabled = false;
//		ActivePart.GetComponent<UISprite>().color = new Color(51.0f/255,51.0f/255,51.0f/255);
//
//
//		ActivePoints = pointsObject;
//
//		foreach (Transform pt in Points)
//			pt.gameObject.SetActive(false);
//
//		foreach (Transform pt in RedPoints)
//			pt.gameObject.SetActive(false);
//
//
//		if(isPointsEnabled)
//			foreach (Transform pt in pointsObject.GetComponent<PointsGroup>().SubPressurePoints)
//				pt.gameObject.SetActive (true);
//		else
//			foreach (Transform pt in pointsObject.GetComponent<PointsGroup>().SubPressurePoints)
//				pt.gameObject.SetActive (false);
//	}
//
//	public void DisplayDetails(Transform source,List<Transform> Links){
//		Details.gameObject.SetActive (true);
//		PanPosition = Vector3.zero;
//		Details.Source = source;
//		Details.Links = Links;
//		Details.Init ();
//	}

	public void StartTut(){

	}

}

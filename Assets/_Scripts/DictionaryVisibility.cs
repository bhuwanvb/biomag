﻿using UnityEngine;
using System.Collections;

public class DictionaryVisibility : MonoBehaviour {
	
	public GameObject Dictionary;
	public GameObject DictionaryCollider;
	
    bool _isShown = false;

    void OnEnable()
	{
		Dictionary.gameObject.SetActive (false);
		DictionaryCollider.gameObject.SetActive (false);
		EventManager.instance.openDictionaryEvent += ShowDictionary;
		EventManager.instance.closeDictionaryEvent += HideDictionary;
    }

   // void OnDisable()
	void OnDestroy()
    {
        if(EventManager.instance != null)
        {
			EventManager.instance.openDictionaryEvent -= ShowDictionary;
			EventManager.instance.closeDictionaryEvent -= HideDictionary;
        }

    }

	private void ToggleVisibility(bool isShown)
    {
		_isShown = isShown;
        if (_isShown)
            HideDictionary();
        else
            ShowDictionary();
    }

    private void ShowDictionary()
    {
        Debug.Log("ShowDictionary");
		Dictionary.gameObject.SetActive (true);
		DictionaryCollider.gameObject.SetActive (true);
    }

    private void HideDictionary()
    {
        Debug.Log("HideDictionary");
		Dictionary.gameObject.SetActive (false);
		DictionaryCollider.gameObject.SetActive (false);
    }
}

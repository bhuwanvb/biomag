﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EventManager : MonoBehaviour
{
    //Singleton instantiation for Manager
    private static EventManager eventManager;
	//public  EventManager eventManager;
	
		//Dictionary button
	public Button DictionaryOpenBtn;
	public Button DictionaryCloseBtn;
	public Button DropdownCloseBtn;
	public Button ResetButton;

	bool NavBool,SlideBool;

	Vector3 originalposition;


	//Menu
	//public Button MenuBtn;
	//public GameObject PathMenu;
	public bool showMenu;
	public bool levelbool = true;

	//public GameObject navigation;
	//public GameObject navigationCollider;

	public GameObject loginScreen,SlideButton;


	
	//enable or disable pressure point 
	public Button EnableScanPointsBtn;
	//public bool showScanpoints;

	public GameObject ScanpointDropdown;
	GameObject[] sp;
	GameObject[] cp;

		//Focuss body parts buttons
	public AccuCamera cameraControl;
    EventManager()
    {
    }

    public static EventManager instance
    {
        get
        {
            if(eventManager == null)
                eventManager = GameObject.FindObjectOfType<EventManager>();
            return eventManager;
        }
    }

    void Awake()
    {
	
      DontDestroyOnLoad (transform.gameObject);
	
        if (eventManager != null && eventManager != this)
        {
            DestroyImmediate(gameObject);
        } else
        {
            eventManager = this;
            DontDestroyOnLoad(instance.gameObject);
        }
    }
	
	void Start()
	{
		NavBool = false;
		SlideBool = false;
		showMenu = false;
    	loginScreen.SetActive (true);
		levelbool = true;

	//	navigationCollider.SetActive (false);

		//ScanpointDropdown = GameObject.Find ("ScanPointDropDown");
		ScanpointDropdown = GameObject.Find ("AccordionScanpoints");

		if(ScanpointDropdown.activeSelf)
				ScanpointDropdown.SetActive (false);
		
		
		
		
				
		DictionaryOpenBtn.onClick.AddListener (()=> { DictionaryButtonPressed(); });
		DictionaryCloseBtn.onClick.AddListener (()=> { CloseDictionaryButtonPressed(); });
		DropdownCloseBtn.onClick.AddListener (()=> { CloseDropDown(ScanpointDropdown); });
		//MenuBtn.onClick.AddListener (() => { ExpandMenu(PathMenu); });
		//EnableScanPointsBtn.onClick.AddListener (() => { EnablePressurePoints(); });
		ResetButton.onClick.AddListener (()=> { ResetButtonPressed(); });

	//	sp = GameObject.FindGameObjectsWithTag ("ScanPoints");
	//	cp = GameObject.FindGameObjectsWithTag ("CorrespondingPoints");
				//EnablePressurePoints ();

	}



    //UICanvas Delegates and Events

    public delegate void UIButtonPressEvent();
	
	//Dictionary events
    public event UIButtonPressEvent openDictionaryEvent;
	public event UIButtonPressEvent closeDictionaryEvent;

    public event UIButtonPressEvent menuButtonEvent;
    public event UIButtonPressEvent instructionButtonEvent;
    public event UIButtonPressEvent resetButtonEvent;
    public event UIButtonPressEvent genderButtonEvent;
    public event UIButtonPressEvent pointsTogglebuttonEvent;
    public event UIButtonPressEvent showButtonsEvent;
	
	

    public delegate void SectionButtonsEvent(string SectionName);
    public event SectionButtonsEvent sectionsButtonPressed;

    public void DictionaryButtonPressed()
    {
		openDictionaryEvent();
    }
	
	public void CloseDictionaryButtonPressed()
	{
		closeDictionaryEvent ();
	}

    public void GenderToggleButtonPressed()
    {
        genderButtonEvent();
    }
	
	public void CloseDropDown(GameObject dropdown)
	{
			Debug.Log ("Close item pressed");
			dropdown.SetActive (false);
	}

	public void ExpandMenu()
	{
			showMenu = !showMenu;
				if (showMenu) {
						AnimatorAssembly.AnimatorController.BottomMenuON ();
					}
				else if (!showMenu) {
					AnimatorAssembly.AnimatorController.BottomMenuOFF ();
				} 
			
	}
	
//	public void EnablePressurePoints()
//	{
//		showScanpoints = !showScanpoints;
//		ScanpointDropdown.SetActive(false);
//		foreach (GameObject go in sp)
//		{
//						go.GetComponent<MeshRenderer> ().enabled = showScanpoints;
//						go.GetComponent<BoxCollider> ().enabled = showScanpoints;
//		}
//		
//		foreach (GameObject go in cp)
//		{
//						go.GetComponent<MeshRenderer> ().enabled = showScanpoints;
//		}
//	}

   // For Navigation Menu Function
	public void EnableNavigationdrawer()
	{
				NavBool = !NavBool;
						if (NavBool) {
								AnimatorAssembly.AnimatorController.NavigationOnAnimation ();
						SlideButton.SetActive (false);
						}
				        else if (!NavBool) {
						
								AnimatorAssembly.AnimatorController.NavigationOFFAnimation ();
						SlideButton.SetActive (true);
						}                     
		}

	// For SlideMenu function
		public void SlideMenuFunction()
		{
				SlideBool = !SlideBool;
				{ 
						if (SlideBool) {
								AnimatorAssembly.AnimatorController.SlideMenuMenuON ();
						}
						else if (!SlideBool) {
								AnimatorAssembly.AnimatorController.SlideMenuOFF ();
						}                     
				}
		}



	 public void ResetButtonPressed()
	{
		//showScanpoints = false;
		ScanpointDropdown.SetActive(false);
	}
	public void SignOut()
	{
		//loginScreen.SetActive (true);
	}



}











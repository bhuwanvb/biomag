/* Implement the circular scrolling list on Unity3D.
 * Author: LanKuDot < https://github.com/LanKuDot; Rain2Wings7@gmail.com >
 */
using UnityEngine;
using System.Collections;

public class ListBox: MonoBehaviour
{
	public int listBoxID;
	public int numOfListBox;

	private SpriteRenderer spriteRenderer;

	private bool keepSliding = false;

	private Vector2 currentMousePos;
	private Vector3 lastWorldMousePos;
	private Vector3 currentWorldMousePos;
	private Vector2 deltaWorldMousePosY;
	private Vector2 maxWorldPos;
	private Vector2 unitWorldPosY;
	private Vector2 beyondPosY;
	private Vector2 lowerBoundPosY;
	private Vector2 upperBoundPosY;
	private Vector2 rangeBoundPosY;
	private int slidingFrames;

	void Start()
	{
		spriteRenderer = GetComponent<Renderer>() as SpriteRenderer;

		// Get the maximum of the world position.
		maxWorldPos = (Vector2) Camera.main.ScreenToWorldPoint(
			new Vector2( Camera.main.pixelWidth, Camera.main.pixelHeight) );

		// Equally divide the range Y
		unitWorldPosY = new Vector2( 0.0f, maxWorldPos.y / 3.0f );

		// Calulate the boundary position
		lowerBoundPosY = new Vector2( 0.0f, unitWorldPosY.y * (float)( -1 * numOfListBox / 2 - 1 ) );
		upperBoundPosY = new Vector2( 0.0f, unitWorldPosY.y * (float)( numOfListBox / 2 + 1 ) );
		rangeBoundPosY = new Vector2( 0.0f, unitWorldPosY.y * (float)( numOfListBox ) );

		// Set the initial position accroding to ListBoxID
		updatePosition( new Vector3( 0.0f, unitWorldPosY.y * (float)( listBoxID * -1 + (int)numOfListBox / 2 ) ) );
	}

	void Update()
	{
		// Click start. Initialize the last world position of the mouse position.
		if ( Input.GetMouseButtonDown(0) )
		{
			currentMousePos = Input.mousePosition;
			lastWorldMousePos = Camera.main.ScreenToWorldPoint( currentMousePos );
		}
		// Get the delta value of the mouse position and move the gameObject with the same value.
		else if ( Input.GetMouseButton(0) )
		{
			currentMousePos = Input.mousePosition;
			currentWorldMousePos = Camera.main.ScreenToWorldPoint( currentMousePos );
			deltaWorldMousePosY = new Vector2( 0.0f, currentWorldMousePos.y - lastWorldMousePos.y );
			updatePosition( (Vector3)deltaWorldMousePosY );

			keepSliding = true;
			slidingFrames = 20;

			lastWorldMousePos = currentWorldMousePos;
		}
		// Slide effect
		else if ( keepSliding )
		{
			--slidingFrames;
			if ( slidingFrames == 0 )
			{
				keepSliding = false;
				return;
			}

			updatePosition( (Vector3)deltaWorldMousePosY );
			deltaWorldMousePosY.y = deltaWorldMousePosY.y / 1.2f;
		}
	}

	/* Update the position of the gameObject when touched.
	 * Curve moving effect.
	 */
	void updatePosition( Vector3 deltaPosition )
	{
		spriteRenderer.transform.position += deltaPosition;
		updateXPosition();
		checkBoundary();
	}

	/* Calculate the x position accroding to the y position.
	 */
	void updateXPosition()
	{
		spriteRenderer.transform.position = new Vector3(
			maxWorldPos.x * 0.5f - maxWorldPos.x * 0.2f * Mathf.Cos( spriteRenderer.transform.position.y / maxWorldPos.y * Mathf.PI / 2.0f ),
			spriteRenderer.transform.position.y );
	}
	
	/* Check if the world position of the GameObject is out of the boundary.
	 * If does, make the GameObject appear at the other slide.
	 */
	void checkBoundary()
	{
		if ( spriteRenderer.transform.position.y < lowerBoundPosY.y )
		{
			beyondPosY = new Vector2( 0.0f, (lowerBoundPosY.y - spriteRenderer.transform.position.y) % rangeBoundPosY.y );
			spriteRenderer.transform.position = new Vector2(
				spriteRenderer.transform.position.x, upperBoundPosY.y - unitWorldPosY.y - beyondPosY.y );
			updateXPosition();
		}
		else if ( spriteRenderer.transform.position.y > upperBoundPosY.y )
		{
			beyondPosY = new Vector2( 0.0f, (spriteRenderer.transform.position.y - upperBoundPosY.y) % rangeBoundPosY.y );
			spriteRenderer.transform.position = new Vector2(
				spriteRenderer.transform.position.x, lowerBoundPosY.y + unitWorldPosY.y + beyondPosY.y );
			updateXPosition();
		}
	}
}

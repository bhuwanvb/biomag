﻿// Copyright 2014 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider))]
public class VRPointer : MonoBehaviour {
	private Vector3 startingPosition;

	public GameObject ARCam,VRCam;
	bool isHit;
	public Transform loader;

	void Start() {
		Screen.sleepTimeout = 0;
		SetGazedAt(false);
	}

	public void OnEnable2(){
		CancelInvoke ();
		print ("reset");

		StopCoroutine ("Loader");

		loader.transform.parent.gameObject.SetActive(false);
		loader.localScale=new Vector3(0,1,1);
	}
	
	public void SetGazedAt(bool gazedAt) {
		print (gazedAt);

		if (gazedAt) {
			InvokeRepeating("Load",0,Time.deltaTime);
			//StartCoroutine("Loader");
		} else
			OnEnable2 ();
	}
	

	public void ToggleVRMode() {
		//Cardboard.SDK.VRModeEnabled = !Cardboard.SDK.VRModeEnabled;
	}

	void Load(){
		loader.transform.parent.gameObject.SetActive (true);
		loader.localScale += Vector3.right * 0.05f;
		
		if (loader.localScale.x > 1) {
			loader.transform.parent.gameObject.SetActive(false);
			loader.localScale=new Vector3(0,1,1);
			if(!ARCam){
				//LoaderLevel.level=2;
				Application.LoadLevel("LoadingScreen");
			}
			ARCam.SetActive (true);
			VRCam.SetActive (false);
			CancelInvoke ();
		}
	}

	IEnumerator Loader(){
		print ("Loading");
		while (loader.localScale.x < 1) {
			loader.transform.parent.gameObject.SetActive (true);
			loader.localScale += Vector3.right * 0.01f;
			yield return new WaitForSeconds(Time.deltaTime);
		}
		//if (loader.localScale.x > 1) {
		loader.transform.parent.gameObject.SetActive(false);
		loader.localScale=new Vector3(0,1,1);

			ARCam.SetActive (true);
			VRCam.SetActive (false);
		//}
	}

}
